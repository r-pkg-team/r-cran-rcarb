Source: r-cran-rcarb
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-rcarb
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-rcarb.git
Homepage: https://cran.r-project.org/package=RCarb
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-interp (>= 1.1),
               r-cran-matrixstats (>= 0.62.0)
Testsuite: autopkgtest-pkg-r

Package: r-cran-rcarb
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R dose rate modelling of carbonate-rich samples
 Translation of the 'MATLAB' program 'Carb' (Nathan and Mauz 2008
 <DOI:10.1016/j.radmeas.2007.12.012>; Mauz and Hoffmann 2014) for dose
 rate modelling for carbonate-rich samples in the context of trapped
 charged dating (e.g., luminescence dating) applications.
